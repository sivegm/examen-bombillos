<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Storage;

class Adext
{
    public static function txtToArray($data)
    {
        $data = str_replace("1", -1, $data);
        $data = array_map('trim',explode("\n",$data));
        $array = [];
        foreach ($data as $d) {
            $values = array_map('trim',explode(",",$d));
            $values = array_map('intval',$values);
            if( count($values) > 1 ) $array[] = $values;
        }
        return $array;
    }

    public static function maxArray($array){
        $res = [];
        $max = max(array_map('max', $array));
        foreach ($array as $r => $row) {
            foreach ($row as $k => $value) {
                if($max == $value){
                    $res['x'] = $k;
                    $res['y'] = $r;
                    $res['value'] = $value;
                }
            }
        }
        return $res;
    }

    public static function isPositive($array){
        $positive = false;
        foreach ($array as $r => $row) {
            foreach ($row as $k => $value) {
                if($value > 0) $positive = true;
            }
        }
        return $positive;
    }

    public static function resolveArray($data){
        $array = self::arrayWeight($data);

        $tot = 0;
        while (self::isPositive($array)) {
            try {
                $max = self::maxArray($array);
                $array = self::disableData($array, $max['x'], $max['y']);
                $array = self::arrayWeight($array);
                $data[$max['y']][$max['x']] = 'X';
                $tot++;
            } catch (\Exception $e) {
            }
        }
        foreach ($array as $r => $row) {
            foreach ($row as $k => $value) {
                if($value == 0){
                    $data[$r][$k] = 'X';
                    $tot++;
                }
            }
        }
        return $data;
    }

    private static function disableData($data, $x, $y){
        //Horizontal
        for ($i=$x; $i<count($data[$x]); $i++) {
            if($data[$y][$i] == -1) break;
            $data[$y][$i] = -1;
        }

        //Horizontal Inversa
        for ($l=$x; $l>0; $l--) {
            if($data[$y][$l-1] == -1) break;
            $data[$y][$l-1] = -1;
        }

        //Vertical
        for ($j=$y+1; $j < count($data); $j++) {
            if($data[$j][$x] == -1)break;
            $data[$j][$x] = -1;
        }

        //Vertical Inversa
        for ($m=$y; $m > 0; $m--) {
            if($data[$m-1][$x] == -1) break;
            $data[$m-1][$x] = -1;
        }
        return $data;
    }

    private static function arrayWeight($data){
        $array = [];
        foreach ($data as $r => $row) {

            foreach ($row as $k => $value) {

                if($value >= 0){
                    $cuantos = 0;
                    //Horizontal
                    for ($i=$k+1; $i < count($row); $i++) {
                        if($data[$r][$i] == -1) break;
                        $cuantos++;
                    }

                    //Horizontal inversa
                    if($k>0){
                        for ($l=$k; $l>0; $l--) {
                            if($data[$r][$l-1] == -1) break;
                            $cuantos++;
                        }
                    }

                    //Vertical
                    for ($j=$r+1; $j < count($data); $j++) {
                        if($data[$j][$k] == -1) break;
                        $cuantos++;
                    }

                    //Vertical inversa
                    if($r>0){
                        for ($m=$r; $m > 0; $m--) {
                            if($data[$m-1][$k] == -1)break;
                            $cuantos++;
                        }
                    }

                    $array[$r][$k] = $cuantos;
                } else {
                    $array[$r][$k] = -1;
                }

            }
        }

        return $array;
    }
}
