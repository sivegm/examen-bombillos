<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Helpers\Adext;

class HomeController extends Controller
{
    public function index()
    {
        return view('welcome');
    }

    public function store(Request $request)
    {
        $request->validate([
            'file' => 'required|file'
        ]);

        $array = Adext::txtToArray($request->file('file')->getContent());

        session(['array' => $array]);

        return view('show', compact('array'));

    }

    public function solve()
    {
        $array = Adext::resolveArray(session('array'));

        return view('show', compact('array'));
    }


}
