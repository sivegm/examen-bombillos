### Instalación
1. Dentro de la carpeta del proyecto, crear el archivo .env
```php
$ cp .env.example .env
```
2. Ejecutar los comandos
```php
$ composer install
$ php artisan key:generate
$ php artisan storage:link
```
3. Ingresar a la url: http://localhost:8000/
4. Cargar un archivo txt que contenga la matriz a solucionar, existe un archivo de prueba en /storage/matriz.txt
