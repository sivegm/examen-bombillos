@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col">
            <h1>Prueba lógica</h1>
            <h3 class="mt-4">Distribucion de bombillos</h3>
            <p>Un electricista muy cuidadoso está tratando de iluminar al más bajo costo posible las habitaciones de sus clientes. Las habitaciones que él ilumina, siempre son habitaciones en forma de matriz (Ver figura 1). Como los bombillos son muy costosos, él trata de iluminar toda la habitación utilizando la menor cantidad de los mismos.</p>
            <p>Los bombillos sólo tienen alcance para iluminar la habitación de forma horizontal y de forma vertical (Ver figura 2 y figura 3).</p>
            <p>Lo único malo es que las habitaciones pueden tener paredes dentro de ellas, en cuyo caso, se interrumpiría el alcance de la luz de un bombillo determinado (Ver figura 4).</p>
            <img class="order-1 w-75" src="{{ asset('storage/bombillas.png') }}" alt="image">
            <p>Usted debe realizar un programa en cualquier lenguaje de programacion, que muestre un menú para las siguientes opciones:</p>
            <p>1.- Cargar habitación: en esta opción el programa deberá cargar un archivo txt que contenga la matriz de uno y ceros que representa la habitación, donde los 0 representan la zona de la habitación que está sin pared y los 1 las zonas en donde existe pared.</p>
            <p>2.- Mostrar colocación de Bombillos Óptimos: En esta opción el programa deberá mostrar la solución de la menor cantidad de bombillos que usted pudo colocar para iluminar toda la habitación. La forma de mostrarlo por pantalla queda a su imaginación, pero se debe ver claramente donde están las paredes y donde se colocaron los bombillos.</p>
            <div class="card">
                <div class="card-body">
                    <form method="POST" action="{{route('home.store')}}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <input type="file" name="file" id="file" accept="text/plain">
                        </div>
                        <button type="submit" class="btn btn-success">Subir archivo txt</button>
                    </form>
                    @if ($errors->any())
                        <div class="alert alert-danger mt-4" role="alert">
                            Existio un error al cargar el archivo!!!
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

